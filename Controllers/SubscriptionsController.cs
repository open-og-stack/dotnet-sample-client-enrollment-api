﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using ServiceProviderSolution.Interfaces;
using ServiceProviderSolution.Models;

namespace ServiceProviderSolution.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]")]
    public class SubscriptionsController : ControllerBase
    {
        private readonly IEnrollmentService _enrollmentService;

        public SubscriptionsController(IEnrollmentService enrollmentService)
        {
            _enrollmentService = enrollmentService;
        }
        
        /// <summary>
        /// Contains service details to initialize the agent-sdk for enrollment
        /// </summary>
        /// <param name="payload"></param>
        /// <returns>OK</returns>
        [HttpPost("init")]
        public async Task<IActionResult> Init([FromBody] InitRequest payload)
        {
            await _enrollmentService.InitService(payload);
            return Ok();
        }
        
        /// <summary>
        /// Contains encrypted payload to enroll subscriber
        /// </summary>
        /// <param name="payload"></param>
        /// <returns>OK</returns>
        [HttpPost]
        public async Task<IActionResult> Enroll([FromBody] EncryptedEnrollmentRequest payload)
        {
            await _enrollmentService.EnrollService(payload);
            return Ok();
        }

        /// <summary>
        /// Contains subscription details to un-subscribe a subscriber 
        /// </summary>
        /// <param name="id"></param>
        /// <param name="payload"></param>
        /// <returns>OK</returns>
        [HttpDelete("{id}")]
        public async Task<IActionResult> UnEnroll([FromRoute] string id, [FromBody] UnEnrollmentRequest payload)
        {
            await _enrollmentService.UnEnrollService(payload);
            return Ok();
        }
    }
}