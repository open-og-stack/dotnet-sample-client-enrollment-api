﻿using System.Threading.Tasks;
using ServiceProviderSolution.Models;

namespace ServiceProviderSolution.Interfaces
{
    public interface ITenantService
    {
        Task<string> GetTenantByProfileIdAsync(string profileId);
        Task<string> GetTenantByIdAsync(string tenantId);
        Task DisableTenantAsync(string tenantId);
        Task SetEnrollmentAsync(EnrollmentOptions payload);
        Task<EnrollmentOptions> GetEnrollmentAsync();
    }
}