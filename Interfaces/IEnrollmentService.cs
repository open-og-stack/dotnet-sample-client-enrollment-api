﻿using System.Threading.Tasks;
using ServiceProviderSolution.Models;

namespace ServiceProviderSolution.Interfaces
{
    public interface IEnrollmentService
    {
        Task InitService(InitRequest payload);
        Task EnrollService(EncryptedEnrollmentRequest payload);
        Task UnEnrollService(UnEnrollmentRequest payload);
    }
}