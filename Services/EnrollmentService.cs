﻿using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using Og_Stack_Agent;
using ServiceProviderSolution.Interfaces;
using ServiceProviderSolution.Models;
using Environment = Og_Stack_Agent.Environment;

namespace ServiceProviderSolution.Services
{
    public class EnrollmentService : IEnrollmentService
    {
        private readonly ILogger<EnrollmentService> _logger;
        private readonly EnrollmentOptions _enrollmentOptions;
        private readonly ITenantService _tenantService;

        public EnrollmentService(ILogger<EnrollmentService> logger, ITenantService tenantService, IOptions<EnrollmentOptions> enrollmentOptions)
        {
            _logger = logger;
            _tenantService = tenantService;
            _enrollmentOptions = enrollmentOptions.Value;
        }

        public async Task InitService(InitRequest payload)
        {
            _logger.LogInformation("Init Payload: {Payload}", JsonConvert.SerializeObject(payload));

            try
            {
                var enrollment = new EnrollmentOptions
                {
                    ConfigurationUrl = _enrollmentOptions.ConfigurationUrl,
                    OperationUrl = _enrollmentOptions.OperationUrl,
                    Environment = _enrollmentOptions.Environment,
                    ServiceName = payload.ServiceName,
                    ServiceProvider = payload.ServiceProviderName,
                    IsSandbox = payload.Sandbox
                };

                //save init payload to db
                await _tenantService.SetEnrollmentAsync(enrollment);
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Error");
            }
        }

        public async Task EnrollService(EncryptedEnrollmentRequest payload)
        {
            _logger.LogInformation("Enroll Payload: {Payload}", JsonConvert.SerializeObject(payload));

             try
            {
                //initialize agent-sdk
                var agent = await GetAgent();
                if (agent is null)
                {
                    _logger.LogInformation("Agent initialization failed");
                    return;
                }

                //decrypt encrypted payload
                var decryptPayload = agent.SecurityManager.DecryptPayload<EnrollmentRequest>(JsonConvert.SerializeObject(payload));
                if (decryptPayload == null)
                {
                    _logger.LogInformation("Error decrypting payload. Decrypted Payload is null");
                    return;
                }
                
                //check if the tenant already exists
                var tenantId = await _tenantService.GetTenantByProfileIdAsync(decryptPayload.ProfileId);
                if (tenantId == null) 
                {
                    //handle logic to enroll the tenant
                }

                //notify og-stack upon successful enrollment of client
                await NotifyServiceCreated(tenantId, decryptPayload.ServiceId, decryptPayload.SubscriptionId, _enrollmentOptions.IsSandbox);
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Error");
            }
        }

        public async Task UnEnrollService(UnEnrollmentRequest payload)
        {
            _logger.LogInformation("UnEnroll Payload: {Payload}", JsonConvert.SerializeObject(payload));

            try
            {
                //payload consists of the parameters passed to operationUrl during enrollment using agent-sdk
                var tenant = await _tenantService.GetTenantByIdAsync(payload.TenantId);
                if (tenant != null)
                {
                    //suspend/soft-delete tenant
                    await _tenantService.DisableTenantAsync(payload.TenantId);
                }
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Error");
            }
        }

        private async Task NotifyServiceCreated(string tenantId, string serviceId, string subscriptionId, bool isSandbox)
        {
            var agent = await GetAgent();
            if (agent == null)
            {
                _logger.LogInformation("Failed initializing Agent SDK");
                return;
            }

            //send service-init event to Og-Stack
            //parameters can be appended to configurationUrl as required
            var configurationUrl = $"{_enrollmentOptions.ConfigurationUrl}?tenantId={tenantId}&serviceId={serviceId}&sandbox={isSandbox}";
            var sendServiceInitialized = await agent.enrolmentManager.SendServiceInitialized(
                subscriptionId, serviceId, new[] { configurationUrl });

            if (!sendServiceInitialized.IsSuccess)
            {
                var response = JsonConvert.SerializeObject(sendServiceInitialized);
                throw new Exception($"Service-Initialized event send failed. ResponseObj: {response}");
            }

            //send service-ready event to Og-Stack
            //parameters can be appended to operationUrl as required (these parameters are sent in Un-enroll service call)
            var operationUrl = $"{_enrollmentOptions.OperationUrl}?tenantId={tenantId}&serviceId={serviceId}&token=[contextual]";
            var sendServiceReady = await agent.enrolmentManager.SendServiceReady(
                subscriptionId, serviceId, new [] {operationUrl});

            if (!sendServiceReady.IsSuccess)
            {
                var response = JsonConvert.SerializeObject(sendServiceReady);
                throw new Exception($"Service-Ready event send failed. Response: {response}");
            }
        }

        private async Task<OgStackAgent> GetAgent()
        {
            //fetch saved init payload from db
            var enrollment = await _tenantService.GetEnrollmentAsync();
            if (enrollment == null)
            {
                _logger.LogInformation("Enrollment fetching failed");
                return null;
            }

            //return instance of Og-Stack
            var environment = Enum.Parse<Environment>(enrollment.Environment);
            return new Builder()
                .SetEnvironment(environment)
                .SetSandbox(enrollment.IsSandbox)
                .SetService(enrollment.ServiceName)
                .SetServiceProvider(enrollment.ServiceProvider)
                .Build();
        }
    }
}