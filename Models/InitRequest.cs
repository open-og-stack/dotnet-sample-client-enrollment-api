﻿using Newtonsoft.Json;

namespace ServiceProviderSolution.Models
{
    public class InitRequest
    {
        [JsonProperty("serviceName")]
        public string ServiceName { get; set; }
        [JsonProperty("serviceProviderName")]
        public string ServiceProviderName { get; set; }
        [JsonProperty("sandbox")]
        public bool Sandbox { get; set; }
        [JsonProperty("environment")]
        public string Environment { get; set; }
    }
}