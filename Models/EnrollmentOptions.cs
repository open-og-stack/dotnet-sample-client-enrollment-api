﻿namespace ServiceProviderSolution.Models
{
    public class EnrollmentOptions
    {
        public const string Enrollment = "Enrollment";

        public string ConfigurationUrl { get; set; }
        public string OperationUrl { get; set; }
        public string ServiceName { get; set; }
        public string ServiceProvider { get; set; }
        public string Environment { get; set; }
        public bool IsSandbox { get; set; }
    }
}