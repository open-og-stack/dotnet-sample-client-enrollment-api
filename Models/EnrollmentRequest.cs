﻿using System.ComponentModel.DataAnnotations;
using Newtonsoft.Json;

namespace ServiceProviderSolution.Models
{
    public class EnrollmentRequest
    {
        [Required]
        [JsonProperty("ogProfileId")]
        public string ProfileId { get; set; }

        [Required]
        [JsonProperty("serviceId")]
        public string ServiceId { get; set; }

        [Required]
        [JsonProperty("subscriptionId")]
        public string SubscriptionId { get; set; }
        
        [JsonProperty("bid")]
        public string Bid { get; set; }

        [JsonProperty("emailID")]
        public string EmailId { get; set; }

        [JsonProperty("country")]
        public string Country { get; set; }

        [JsonProperty("region")]
        public string Region { get; set; }

        [JsonProperty("city")]
        public string City { get; set; }

        [JsonProperty("address")]
        public string Address { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("executionContext")]
        public string ExecutionContext { get; set; }

        [JsonProperty("serviceName")]
        public string ServiceName { get; set; }

        [JsonProperty("serviceProviderName")]
        public string ServiceProviderName { get; set; }
    }
}