﻿using Newtonsoft.Json;

namespace ServiceProviderSolution.Models
{
    public class EncryptedEnrollmentRequest
    {
        [JsonProperty("encryptedData")]
        public string EncryptedData { get; set; }
        [JsonProperty("encryptedKey")]
        public string EncryptedKey { get; set; }
        [JsonProperty("publicKeyFingerprint")]
        public string PublicKeyFingerprint { get; set; }
        [JsonProperty("hashingAlgorithm")]
        public string HashingAlgorithm { get; set; }
    }
}