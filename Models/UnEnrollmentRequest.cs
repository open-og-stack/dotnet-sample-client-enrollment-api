﻿using Newtonsoft.Json;

namespace ServiceProviderSolution.Models
{
    public class UnEnrollmentRequest
    {
        [JsonProperty("serviceId")]
        public string ServiceId { get; set; }
        [JsonProperty("tenantId")]
        public string TenantId { get; set; }
        [JsonProperty("reason")] 
        public string Reason { get; set; }
    }
}